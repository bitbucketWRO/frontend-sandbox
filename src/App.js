import "./App.css";

function App() {
  const posts = [
    {
      userId: 1,
      id: 1,
      title:
        "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      body: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto",
    },
    {
      userId: 1,
      id: 2,
      title: "qui est esse",
      body: "est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla",
    },
    {
      userId: 1,
      id: 3,
      title: "ea molestias quasi exercitationem repellat qui ipsa sit aut",
      body: "et iusto sed quo iure voluptatem occaecati omnis eligendi aut ad voluptatem doloribus vel accusantium quis pariatur molestiae porro eius odio et labore et velit aut",
    },
  ];
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="wrapper wrapper-content animated fadeInRight">
            <div className="ibox-content m-b-sm border-bottom">
              <div className="p-xs">
                <div className="pull-left m-r-md">
                  <i className="fa fa-globe text-navy mid-icon"></i>
                </div>
                <h2>Welcome to our forum</h2>
                <span>Feel free to choose topic you're interested in.</span>
              </div>
            </div>

            <div className="ibox-content forum-container">
              <div className="forum-title">
                <div className="pull-right forum-desc">
                  <samll>Total posts: 320,800</samll>
                </div>
                <h3>General subjects</h3>
              </div>

              <div className="forum-item">
                <div className="row">
                  <div className="col-md-9">
                    <div className="forum-icon">
                      <i className="fa fa-star"></i>
                    </div>
                    <a href="forum_post.html" className="forum-item-title">
                      Title Here
                    </a>
                    <div className="forum-sub-title">Body here</div>
                  </div>
                  <div className="col-md-1 forum-info">
                    <span className="views-number">1</span>
                    <div>
                      <small>Post ID</small>
                    </div>
                  </div>
                  <div className="col-md-1 forum-info">
                    <span class="views-number">1</span>
                    <div>
                      <small>User ID</small>
                    </div>
                  </div>
                </div>
              </div>

              <div class="forum-item">
                <div class="row">
                  <div class="col-md-9">
                    <div class="forum-icon">
                      <i class="fa fa-star"></i>
                    </div>
                    <a href="forum_post.html" class="forum-item-title">
                      Title Here
                    </a>
                    <div class="forum-sub-title">Body here</div>
                  </div>
                  <div class="col-md-1 forum-info">
                    <span class="views-number">1</span>
                    <div>
                      <small>Post ID</small>
                    </div>
                  </div>
                  <div class="col-md-1 forum-info">
                    <span class="views-number">1</span>
                    <div>
                      <small>User ID</small>
                    </div>
                  </div>
                </div>
              </div>

              <div class="forum-item">
                <div class="row">
                  <div class="col-md-9">
                    <div class="forum-icon">
                      <i class="fa fa-star"></i>
                    </div>
                    <a href="forum_post.html" class="forum-item-title">
                      Title Here
                    </a>
                    <div class="forum-sub-title">Body here</div>
                  </div>
                  <div class="col-md-1 forum-info">
                    <span class="views-number">1</span>
                    <div>
                      <small>Post ID</small>
                    </div>
                  </div>
                  <div class="col-md-1 forum-info">
                    <span class="views-number">1</span>
                    <div>
                      <small>User ID</small>
                    </div>
                  </div>
                </div>
              </div>

              <div class="forum-item">
                <div class="row">
                  <div class="col-md-9">
                    <div class="forum-icon">
                      <i class="fa fa-star"></i>
                    </div>
                    <a href="forum_post.html" class="forum-item-title">
                      Title Here
                    </a>
                    <div class="forum-sub-title">Body here</div>
                  </div>
                  <div class="col-md-1 forum-info">
                    <span class="views-number">1</span>
                    <div>
                      <small>Post ID</small>
                    </div>
                  </div>
                  <div class="col-md-1 forum-info">
                    <span class="views-number">1</span>
                    <div>
                      <small>User ID</small>
                    </div>
                  </div>
                </div>
              </div>

              <div className="forum-item">
                <div className="row">
                  <div className="col-md-9">
                    <div className="forum-icon">
                      <i className="fa fa-star"></i>
                    </div>
                    <a href="forum_post.html" className="forum-item-title">
                      Title Here
                    </a>
                    <div className="forum-sub-title">Body here</div>
                  </div>
                  <div className="col-md-1 forum-info">
                    <span className="views-number">1</span>
                    <div>
                      <small>Post ID</small>
                    </div>
                  </div>
                  <div className="col-md-1 forum-info">
                    <span className="views-number">1</span>
                    <div>
                      <small>User ID</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-12">Footer here</div>
    </div>
  );
}

export default App;
